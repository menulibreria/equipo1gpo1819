
package Libreria;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class V_FrmDevoluciones extends JFrame {
    public V_FrmDevoluciones(){
        marco();
    }
    public void marco(){
    this.setSize(400,400);
    this.setTitle("Devoluciones de libros");
    this.setLocation(0,0);
    this.setVisible(true);
    
    
    JPanel panelMayor = new JPanel(new BorderLayout());
    
     
        JPanel panelNorte = new JPanel();
        JPanel panelCentro = new JPanel();
        JPanel panelSur= new JPanel();
        //JPanel panelEste= new JPanel();
        //JPanel panelOeste= new JPanel();
        
        
         FlowLayout fl1 = new FlowLayout(FlowLayout.CENTER);
        panelNorte.setLayout(fl1);
        
        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();
        panelCentro.setLayout(gbl);
        
        FlowLayout fl2 = new FlowLayout();
        panelSur.setLayout(fl2);
        
        /*GridLayout gl2 = new GridLayout(3,0);
        panelEste.setLayout(gl2);
        
        GridLayout gl3 = new GridLayout(3,0);
        panelOeste.setLayout(gl3);*/
        
        JLabel lbl1Titulo = new JLabel("SOLICITUD DE LIBRO");
        
        JLabel lbl1 = new JLabel("Codigo del libro:");
        JTextField tf1 = new JTextField(25);
        JLabel lbl2 = new JLabel("E-mail:");
        JTextField tf2 = new JTextField(25);
        JTextArea ta1 = new JTextArea("Los libros electrónicos del Equipo 1\nno están sujetos a la política\nde cancelación ni a la política "
                + "\nde devolución de los libros impresos. "
                + "\nEl Equipo 1 no permite que los "
                + "\nusuarios cancelen pedidos, obtengan reembolsos "
                + "\no reciban sustituciones de libros electrónicos.");  
        
        
        JButton bs1 = new JButton();
        JButton bs2 = new JButton();
        bs1.setText("Aceptar");
        
        bs2.setText("Cancelar");
       /* bs2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
             
            }
        });
        */
        
        
        panelNorte.add(lbl1Titulo);
         
        gbc.gridx=0;
        gbc.gridy=0;
        
        gbc.anchor = GridBagConstraints.WEST;
        panelCentro.add(lbl1,gbc);
        gbc.gridy=1;
        gbc.gridwidth=5;
        panelCentro.add(tf1,gbc);
        gbc.gridwidth=1;
        gbc.gridy=2;
        panelCentro.add(lbl2,gbc);
        gbc.gridy=3;
        gbc.gridwidth=5;
        panelCentro.add(tf2,gbc);
        gbc.gridwidth=1;
        gbc.gridy=4;
        ta1.setEditable(false);
         panelCentro.add(ta1,gbc);
        
  
        
        panelSur.add(bs1);
        panelSur.add(bs2);
        
        
       /* panelOeste.add(bo1);
        panelOeste.add(bo2);
        panelOeste.add(bo3);
        
        panelEste.add(be1);
        panelEste.add(be2); */
    
        panelMayor.add(panelNorte, BorderLayout.NORTH);
        panelMayor.add(panelCentro, BorderLayout.CENTER);
        panelMayor.add(panelSur, BorderLayout.SOUTH);
        //panelMayor.add(panelEste, BorderLayout.EAST);
        //panelMayor.add(panelOeste, BorderLayout.WEST);
       
        
        this.add(panelMayor);
        
        
        this.setVisible(true);
    
    
    //PRUEBA
}
}