/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Libreria;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Angel
 */
public class V_FrmSolicitud extends JFrame {
    public V_FrmSolicitud(){
        marco();
    }
    public void marco(){
        
       
        this.setSize(500,500);
        this.setTitle("Solicitudes de libros");
        this.setLocation(150,150);
        this.setVisible(true);
    
     JPanel panelMayor = new JPanel(new BorderLayout());
    
     
        JPanel panelNorte = new JPanel();
        JPanel panelCentro = new JPanel();
        JPanel panelSur= new JPanel();
        //JPanel panelEste= new JPanel();
        //JPanel panelOeste= new JPanel();
        
        
         FlowLayout fl1 = new FlowLayout(FlowLayout.CENTER);
        panelNorte.setLayout(fl1);
        
        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();
        panelCentro.setLayout(gbl);
        
        FlowLayout fl2 = new FlowLayout();
        panelSur.setLayout(fl2);
        
        /*GridLayout gl2 = new GridLayout(3,0);
        panelEste.setLayout(gl2);
        
        GridLayout gl3 = new GridLayout(3,0);
        panelOeste.setLayout(gl3);*/
        
        JLabel lbl1Titulo = new JLabel("SOLICITUD DE LIBRO");
        
        JLabel lbl1 = new JLabel("Nombre:");
        JTextField tf1 = new JTextField(25);
        JLabel lbl2 = new JLabel("Direccion:");
        JTextField tf2 = new JTextField(25);
        JLabel lbl3 = new JLabel("Telefono:");
        JTextField tf3 = new JTextField(25);
        JLabel lbl4 = new JLabel("E-mail:");
        JTextField tf4 = new JTextField(25);
        JLabel lbl5 = new JLabel("Solicito el sig. libro en calidad de Prestamo");
        JTextField tf5 = new JTextField(25);
        JLabel lbl6 = new JLabel("Codigo del libro");
        JTextField tf6 = new JTextField(25);
        
        JButton bs1 = new JButton();
        JButton bs2 = new JButton();
        bs1.setText("Aceptar");
        bs2.setText("Cancelar");
        
        panelNorte.add(lbl1Titulo);
         
        gbc.gridx=0;
        gbc.gridy=0;
        
        gbc.anchor = GridBagConstraints.WEST;
        panelCentro.add(lbl1,gbc);
        gbc.gridy=1;
        gbc.gridwidth=5;
        panelCentro.add(tf1,gbc);
        gbc.gridwidth=1;
        gbc.gridy=2;
        panelCentro.add(lbl2,gbc);
        gbc.gridy=3;
        gbc.gridwidth=5;
        panelCentro.add(tf2,gbc);
        gbc.gridwidth=1;
        gbc.gridy=4;
        panelCentro.add(lbl3,gbc);
        gbc.gridy=5;
        gbc.gridwidth=5;
        panelCentro.add(tf3,gbc);
        gbc.gridwidth=1;
        gbc.gridy=6;
        panelCentro.add(lbl4,gbc);
        gbc.gridwidth=5;
        gbc.gridy=7;
        panelCentro.add(tf4,gbc);
        gbc.gridy=8;
        gbc.gridwidth=1;
        panelCentro.add(lbl5,gbc);
        gbc.gridy=9;
        gbc.gridwidth=5;
        panelCentro.add(tf5,gbc);
        gbc.gridy=10;
        gbc.gridwidth=1;
        panelCentro.add(lbl6,gbc);
        gbc.gridwidth=5;
        gbc.gridy=11;
        panelCentro.add(tf6,gbc);
        
        panelSur.add(bs1);
        panelSur.add(bs2);
        
        
       /* panelOeste.add(bo1);
        panelOeste.add(bo2);
        panelOeste.add(bo3);
        
        panelEste.add(be1);
        panelEste.add(be2); */
    
        panelMayor.add(panelNorte, BorderLayout.NORTH);
        panelMayor.add(panelCentro, BorderLayout.CENTER);
        panelMayor.add(panelSur, BorderLayout.SOUTH);
        //panelMayor.add(panelEste, BorderLayout.EAST);
        //panelMayor.add(panelOeste, BorderLayout.WEST);
       
        
        this.add(panelMayor);
        
        
        this.setVisible(true);
    
    
    }

}
