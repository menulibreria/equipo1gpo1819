/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Libreria;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;

/**
 *
 * @author Uriel_nlvlerd
 */
public class C_FrmPrestado implements ActionListener {
    DefaultListModel lista=new DefaultListModel();
    private M_FrmPrestado modelo;
    private V_FrmPrestado vista;
   
    public C_FrmPrestado(M_FrmPrestado modelo,V_FrmPrestado vista){
         this.modelo=modelo;
         this.vista=vista;
         
         this.vista.Registrar.addActionListener(this);
         this.vista.Cancelar.addActionListener(this);
     }
    public void iniciarVista(){
    vista.setTitle("Libreria");
    vista.pack();
    vista.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    vista.setLocationRelativeTo(null);
    vista.setVisible(true);  
    }
    public void actionPerfomed(ActionEvent evento){
        if(vista.Registrar == evento.getSource()){
               lista.addElement(vista.cp1.getText()+""+
                         vista.cp2.getText()+""+
                         vista.cp3.getText()+"");
                             modelo.setNombre("");
                             modelo.setAutor("");
                             modelo.setCategoria("");
                             vista.lista.setModel(lista); 
        }
        else{}
       
        
    }
    public void actionPerformed(ActionEvent evento){
     if(vista.Cancelar==evento.getSource()){
        modelo.setNombre("");
        modelo.setAutor("");
        modelo.setCategoria("");
        lista.removeAllElements();
        vista.lista.setModel(lista);
        }
     else{}
    }
        
}
