/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Libreria;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author Angel
 */
public class FrmDigital extends JFrame{
    public FrmDigital(){
        marco();
    }
    public void marco(){
        JFrame f = new JFrame("Libros digitales");
        this.setSize(400,400);
        this.setTitle("Libros disponibles a descargar");
        this.setLocation(200,200);
        this.setVisible(true);
        
        JPanel panelMayor = new JPanel(new BorderLayout());
        JPanel panelNorte = new JPanel();
        JPanel panelCentro = new JPanel();
        JPanel panelSur = new JPanel();
        JPanel panelEste = new JPanel();
        
        //creamos el panel norte con alineacion al centro
        FlowLayout  fl1 = new FlowLayout(FlowLayout.CENTER);
        panelNorte.setLayout(fl1);
        
        FlowLayout fl2 = new FlowLayout();
        panelSur.setLayout(fl2);
        
        GridLayout gl2 = new GridLayout(10,0);
        panelEste.setLayout(gl2);
        
        //componnetes del panel norte
        JLabel lblTitulo = new JLabel("Libros en venta");
        
        //COmponenetes para el panel Este
        JTextField tfl = new JTextField(25);
        JButton bs1 = new JButton();
        bs1.setText("Buscar");
        
        //añadiendo componentes al panel principal
        panelNorte.add(lblTitulo);
        
        panelEste.add(tfl);
        panelEste.add(bs1);
        
        //añadiendo los componentes al panel mayor
        panelMayor.add(panelNorte, BorderLayout.NORTH);
        panelMayor.add(panelEste, BorderLayout.EAST);
        
        this.add(panelMayor);
        
    }
    
}
