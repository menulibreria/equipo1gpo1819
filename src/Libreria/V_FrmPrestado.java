package Libreria;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Uriel
 */
public class V_FrmPrestado extends JFrame {
    public JButton Registrar,Cancelar,VerRegistro;
    public JList lista;
    public JLabel  Nombre,Autor,Editorial,Categoria;
    public JTextField cp1,cp2,cp3,cp4;
    DefaultTableModel tab = new DefaultTableModel();
    JTable table=new JTable(tab);
    
    public V_FrmPrestado(){
        //Paneles a necesitar
        JPanel panelMayor=new JPanel(new BorderLayout());
        JPanel panelNorte=new JPanel();
        JPanel panelOeste=new JPanel();
        JPanel panelSur=new JPanel();
                
        //Layout Manager a utilizar 
        FlowLayout fl1 = new FlowLayout(FlowLayout.CENTER);
        panelNorte.setLayout(fl1);
       
        GridBagLayout gbl1 = new GridBagLayout();
        GridBagConstraints gbc1 = new GridBagConstraints();
        panelOeste.setLayout(gbl1);
        
        FlowLayout fl2 = new FlowLayout();
        panelSur.setLayout(fl2);
        
        //Componentes para el panel Norte 
        JLabel jlbl1Titulo=new JLabel("Registro de libros prestados");
        
        //Componenetes para el panel Centro 
        /* JLabel jlb1=new JLabel("Nombre");
        JTextField jtf1=new JTextField(25);
        JLabel jlb2=new JLabel("Categoria");
        JTextField jtf2=new JTextField(25);
        JLabel jlb3=new JLabel("Edicion");
        JTextField jtf3=new JTextField(25);
        JLabel jlb4=new JLabel("Autor");
        JTextField jtf4=new JTextField(25);
        JLabel jlb5=new JLabel("Fecha de salida");
        JTextField jtf5=new JTextField(25);
        JLabel jlb6=new JLabel("Fecha de entrega");
        JTextField jtf6=new JTextField(25);*/
        Nombre =new JLabel("Nombre");
        cp1= new JTextField(25);
        Autor=new JLabel("Autor");
        cp2=new JTextField(25);
        Categoria=new JLabel("Categoria");
        cp3=new JTextField(25);
        Editorial=new JLabel("Editorial");
        cp4=new JTextField(25);
        
        lista=new JList();
        //Componentes para el PanelSur
        /*JButton jb1=new JButton();
        JButton jb2=new JButton();
        jb1.setText("Registrar");
        jb2.setText("Cancelar");*/
        Registrar= new JButton("Registrar");
        Cancelar =new JButton("Cancelar");
        VerRegistro=new JButton("Ver Registro");
        //Componentes
        //Componentes para panel Norte 
        panelNorte.add(jlbl1Titulo);
        
        //Componentes para panel Oeste
        gbc1.gridx=0;
        gbc1.gridy=0;
        gbc1.anchor = GridBagConstraints.CENTER;
        panelOeste.add(Nombre,gbc1);
        gbc1.gridy=1;
        gbc1.gridwidth=5;
        panelOeste.add(cp1,gbc1);
        gbc1.gridy=2;
        gbc1.gridwidth=1;
        panelOeste.add(Autor,gbc1);
        gbc1.gridy=3;
        gbc1.gridwidth=5;
        panelOeste.add(cp2,gbc1);
        gbc1.gridy=4;
        gbc1.gridwidth=1;
        panelOeste.add(Categoria,gbc1);
        gbc1.gridy=5;
        gbc1.gridwidth=5;
        panelOeste.add(cp3,gbc1);
        gbc1.gridy=6;
        gbc1.gridwidth=1;
        panelOeste.add(Editorial,gbc1);
        gbc1.gridy=7;
        gbc1.gridwidth=5;
        panelOeste.add(cp4,gbc1);
        gbc1.gridy=8;
        gbc1.gridwidth=1;
        panelOeste.add(lista,gbc1);
        /*gbc1.gridy=9;
        gbc1.gridwidth=5;
        panelOeste.add(jtf5,gbc1);
        gbc1.gridy=10;
        gbc1.gridwidth=1;
        panelOeste.add(jlb6,gbc1);
        gbc1.gridy=11;
        gbc1.gridwidth=5;
        panelOeste.add(jtf6,gbc1);*/
        
        //Agregacion de componentes al panel SUR
        /*panelSur.add(jb1);
        panelSur.add(jb2);*/
        panelSur.add(Registrar);
        panelSur.add(Cancelar);
        panelSur.add(VerRegistro);
        //Agregar al panel mayor
        panelMayor.add(panelNorte,BorderLayout.NORTH);
        panelMayor.add(panelOeste,BorderLayout.CENTER);
        panelMayor.add(panelSur,BorderLayout.SOUTH);
          
        tab.addColumn("Nombre");
        tab.addColumn("Autor");
        tab.addColumn("Categoria");
        tab.addColumn("Editorial");
       //Asociar a JFRAME
        this.add(panelMayor);
        //Hacemos visibles
        //this.setVisible(true);
        final JFrame t=new JFrame();
        t.setSize(350,350);
        t.setLocation(350,350);
        t.add(new JScrollPane(table));
        t.setVisible(false);
        
        Registrar.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent e){
          tab.addRow(new Object[]{cp1.getText(),cp2.getText(),cp3.getText(),cp4.getText()});
          
          cp1.setText("");
          cp2.setText("");
          cp3.setText("");
          cp4.setText("");
        }
        });
        
        VerRegistro.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent e){
            t.setVisible(true);
        }
        });
        }
}

