package Libreria;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Uriel
 */
public class V_FrmDisponible extends JFrame {
    DefaultListModel jlist=new DefaultListModel();
    public JButton registrar,cancelar,VerRegistro;
    public JList   lista;
    public JLabel  Nombre,Autor,Edicion,Categoria;
    public JTextField cp1,cp2,cp3;
    DefaultTableModel tab = new DefaultTableModel();
    JTable table=new JTable(tab);
    

    public V_FrmDisponible(){
        //Aqui se crea la ventana a mostrar
       
        /*this.setSize(450,450);
        this.setLocation(200,200);
        this.setTitle("Libros Disponibles");*/
       
       //Paneles que van a ser utilizados
        JPanel panelMayor= new JPanel(new BorderLayout());
        JPanel panelNorte =new JPanel();
        JPanel panelCentro =new JPanel();
        JPanel panelSur =new JPanel();
        //Layout Manager a utilizar
        FlowLayout fl1 = new FlowLayout(FlowLayout.CENTER);
        panelNorte.setLayout(fl1);
       
        GridBagLayout gbl1 = new GridBagLayout();
        GridBagConstraints gbc1 = new GridBagConstraints();
        panelCentro.setLayout(gbl1);
        
        FlowLayout fl2 = new FlowLayout();
        panelSur.setLayout(fl2);
        
        //Aqui se crean los componentes par el panel Norte
        JLabel jlbl1Titulo = new JLabel("Registro de Libros Disponibles");
        
        //Aqui se crean los componentes para el panel Centro
        //JLabel jlb1 = new JLabel("Nombre del libro:");
        //JTextField tf1 =new JTextField(25);
        //JLabel jlb2 = new JLabel("Autor:");
        //JTextField tf2=new JTextField(25);
        //JLabel jlb3 = new JLabel("Edicion:");
        //JTextField tf3=new JTextField(25);
        //JLabel jlb4 = new JLabel("Categoria:");
        //JTextField tf4=new JTextField(25);
        //JList  jl1 = new JList();
        Nombre=new JLabel("Nombre");
        cp1=new JTextField(25);
        Autor=new JLabel("Autor");
        cp2=new JTextField(25);
        Categoria=new JLabel("Categoria");
        cp3=new JTextField(25);
        lista=new JList();
        //Aqui creamos los componentes para el panel Sur
        //JButton jb1=new JButton();
        //JButton jb2=new JButton();
        //jb1.setText("Registrar");
        //jb2.setText("Cancelar");
        registrar= new JButton("registrar");
        cancelar = new JButton("cancelar");
        VerRegistro=new JButton("Ver Registros");
        //Agrgacion de los componentes a los paneles
        //Agregacion de componentes al panel Norte
        panelNorte.add(jlbl1Titulo);
        
        //Agregacion de componenetes al panel Oeste
        /*gbc1.gridx=0;
        gbc1.gridy=0;
        gbc1.anchor = GridBagConstraints.CENTER;
        panelCentro.add(jlb1,gbc1);
        gbc1.gridy=1;
        gbc1.gridwidth=5;
        panelCentro.add(tf1,gbc1);
        gbc1.gridy=2;
        gbc1.gridwidth=1;
        panelCentro.add(jlb2,gbc1);
        gbc1.gridy=3;
        gbc1.gridwidth=5;
        panelCentro.add(tf2,gbc1);
        gbc1.gridy=4;
        gbc1.gridwidth=1;
        panelCentro.add(jlb3,gbc1);
        gbc1.gridy=5;
        gbc1.gridwidth=5;
        panelCentro.add(tf3,gbc1);
        gbc1.gridy=6;
        gbc1.gridwidth=1;
        panelCentro.add(jlb4,gbc1);
        gbc1.gridy=7;
        gbc1.gridwidth=5;
        panelCentro.add(tf4,gbc1);
        gbc1.gridy=8;
        gbc1.gridwidth=1;
        panelCentro.add(jl1);*/
        gbc1.gridx=0;
        gbc1.gridy=0;
        gbc1.anchor = GridBagConstraints.CENTER;
        panelCentro.add(Nombre,gbc1);
        gbc1.gridy=1;
        gbc1.gridwidth=5;
        panelCentro.add(cp1,gbc1);
        gbc1.gridy=2;
        gbc1.gridwidth=1;
        panelCentro.add(Autor,gbc1);
        gbc1.gridy=3;
        gbc1.gridwidth=5;
        panelCentro.add(cp2,gbc1);
        gbc1.gridy=4;
        gbc1.gridwidth=1;
        panelCentro.add(Categoria,gbc1);
        gbc1.gridy=5;
        gbc1.gridwidth=5;
        panelCentro.add(cp3,gbc1);
        gbc1.gridy=6;
        gbc1.gridwidth=1;
        panelCentro.add(lista,gbc1);
        //Agregacion de componentes al panel Sur
        panelSur.add(registrar);
        panelSur.add(cancelar);
        panelSur.add(VerRegistro);
        
        //Agregacion al panelMayor
        panelMayor.add(panelNorte,BorderLayout.NORTH);
        panelMayor.add(panelCentro,BorderLayout.CENTER);
        panelMayor.add(panelSur,BorderLayout.SOUTH);
        
       
        
        //Asociar a JFrame
        this.add(panelMayor);
        
        //this.setVisible(true);
        tab.addColumn("Nombre");
        tab.addColumn("Autor");
        tab.addColumn("Categoria");
        
        final JFrame t=new JFrame();
        t.setSize(350,350);
        t.setLocation(350,350);
        t.add(new JScrollPane(table));
        t.setVisible(false);
        
        registrar.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent e){
          tab.addRow(new Object[]{cp1.getText(),cp2.getText(),cp3.getText()});
          
          cp1.setText("");
          cp2.setText("");
          cp3.setText("");
        }
        });
        
        VerRegistro.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent e){
            t.setVisible(true);
        }
        });
    }
    }
